package app;

import app.graph.DirectedGraph;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Class responsible to read the data from a file and
 * create a directed graph from it
 *
 * A new instance should be created for every read session
 * Then getGraph can be used to retrieve the graph
 */
public class MapReader {

    private String path;
    private DirectedGraph directedGraph;

    public static MapReader startRead(String path) {
        MapReader reader = new MapReader();
        reader.path = path;
        return reader;
    }

    public MapReader read() throws FileNotFoundException {
        InputStream in = getClass().getResourceAsStream(this.path);
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

        Scanner scanner = new Scanner(reader);

        DirectedGraph directedGraph = new DirectedGraph();

        int numberOfNodes = scanner.nextInt();
        for (int i = 0; i < numberOfNodes; i++) {
            String osmId = scanner.next();
            directedGraph.addNode(new CmNode(osmId));
        }

        int numberOfEdges = scanner.nextInt();
        for (int i = 0; i < numberOfEdges; i++) {
            String fromId = scanner.next();
            String toId = scanner.next();
            Integer cost = Integer.valueOf(scanner.next());
            DirectedGraph.Edge edge = new DirectedGraph.Edge<>(new CmNode(toId), cost);
            directedGraph.addEdge(new CmNode(fromId), edge);
        }

        this.directedGraph = directedGraph;
        return this;
    }

    public DirectedGraph getGraph() {
        return this.directedGraph;
    }
}
