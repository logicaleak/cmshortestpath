package app.algo;

import java.util.ArrayList;

/**
 * Extending class to remove mutable operations from ArrayList
 * @param <T>
 */
public class ImmutableArrayList <T> extends ArrayList<T> {
    @Override
    public T set(int index, T element) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean add(T t) {
        throw new UnsupportedOperationException();
    }
}
