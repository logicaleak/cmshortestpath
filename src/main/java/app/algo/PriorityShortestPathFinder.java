package app.algo;

import app.graph.DirectedGraph;

import java.util.*;

public class PriorityShortestPathFinder<G extends DirectedGraph> {
    private G graph;
    private PriorityQueue<QueueNode> priorityQueue;
    private Map<DirectedGraph.Node, Best> completed;
    private Map<DirectedGraph.Node, Best> distanceMap;


    private static class Best {
        public final Integer cost;
        public final DirectedGraph.Node from;

        public Best(Integer cost, DirectedGraph.Node from) {
            this.cost = cost;
            this.from = from;
        }
    }

    /**
     * Wrapper class for DirectedGraph.Node
     */
    private static class QueueNode {
        final public Integer shortestPath;
        final public DirectedGraph.Node node;


        public QueueNode(DirectedGraph.Node node, Integer shortestPath) {
            this.node = node;
            this.shortestPath = shortestPath;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof DirectedGraph.Node) {
                return this.node.equals(obj);
            }
            return node.equals(((QueueNode) obj).node);
        }

        @Override
        public int hashCode() {
            return node.hashCode();
        }

        @Override
        public String toString() {
            return node.toString() + " " + shortestPath.toString();
        }
    }

    public PriorityShortestPathFinder(G graph) {
        priorityQueue = new PriorityQueue<>(new Comparator<QueueNode>() {
            @Override
            public int compare(QueueNode o1, QueueNode o2) {
                return o1.shortestPath - o2.shortestPath;
            }
        });

        this.graph = graph;
        this.completed = new HashMap<>();
        this.distanceMap = new HashMap<>();
    }

    private void fillPriorityQueue(DirectedGraph.Node startNode) {
        this.priorityQueue.add(new QueueNode(startNode, 0));
        Iterator<DirectedGraph.Node> iterator = this.graph.iterator();
        while (iterator.hasNext()) {
            DirectedGraph.Node node = iterator.next();
            if (!node.equals(startNode)) {
                this.priorityQueue.add(new QueueNode(node, Integer.MAX_VALUE));
            }
        }
    }

    public static class ShortestPath {
        public List<DirectedGraph.Node> path;
        public Integer totalCost;

        public ShortestPath(List<DirectedGraph.Node> path, Integer totalCost) {
            this.path = path;
            this.totalCost = totalCost;
        }
    }

    /**
     * Traceback the path to retrieve the shortest path once the destination node is reached
     * @param startNode Start node
     * @param destNode Dest node
     * @return Shortest path
     */
    public ShortestPath tracePath(DirectedGraph.Node startNode, DirectedGraph.Node destNode) {
        Integer cost = completed.get(destNode).cost;
        Best currentBest = completed.get(destNode);
        List<DirectedGraph.Node> path = new LinkedList<>();
        path.add(destNode);
        while (true) {
            path.add(currentBest.from);
            if (currentBest.from.equals(startNode)) {
                break;
            }
            currentBest = completed.get(currentBest.from);
        }

        return new ShortestPath(new ImmutableReversedArrayList<>(path), cost);
    }

    public ShortestPath calculateShortestPath(DirectedGraph.Node startNode, DirectedGraph.Node destNode) {
        fillPriorityQueue(startNode);

        QueueNode previousNode = priorityQueue.peek();
        distanceMap.put(previousNode.node, new Best(0, previousNode.node));
        mainLoop:
        while (priorityQueue.iterator().hasNext()) {
            QueueNode currentNode = priorityQueue.remove();

            completed.put(currentNode.node, distanceMap.get(currentNode));

            //We have reached the destination, stop the iteration
            if (currentNode.equals(destNode)) {
                break;
            }

            List<DirectedGraph.Edge> edges = graph.getEdges(currentNode.node);
            //If the node has no edges, move on to another node
            if (edges.size() == 0) {
                continue;
            }
            for (DirectedGraph.Edge edge : edges) {
                DirectedGraph.Node to = edge.to;

                Best currentBest = distanceMap.get(to);
                Best currentBestOfCurrent = distanceMap.get(currentNode);

                //If this is null, the process is reseted and we cannot go from the startNode to endNode ??
                if (currentBestOfCurrent == null) {
                    break mainLoop;
                }
                Integer newCost = edge.cost + currentBestOfCurrent.cost;

                if (currentBest != null) {
                    if (newCost < currentBest.cost) {
                        Boolean removed = priorityQueue.remove(new QueueNode(to, currentBest.cost));
                        priorityQueue.add(new QueueNode(to, newCost));
                    }
                } else {
                    //If this node is arrived for the first time
                    distanceMap.put(to, new Best(newCost, currentNode.node));
                    Boolean removed = priorityQueue.remove(new QueueNode(to, null));
                    priorityQueue.add(new QueueNode(to, newCost));
                }


            }
        }

        return tracePath(startNode, destNode);
    }
}
