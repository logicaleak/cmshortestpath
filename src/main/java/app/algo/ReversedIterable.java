package app.algo;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * This class is used to treat a iterable object as if it was reversed
 * without actually reversing the order to avoid extra time complexity
 * @param <T>
 */
public class ReversedIterable<T> implements Iterable<T> {
    private final List<T> original;

    public ReversedIterable(List<T> original) {
        this.original = original;
    }

    public Iterator<T> iterator() {
        final ListIterator<T> i = original.listIterator(original.size());

        return new Iterator<T>() {
            public boolean hasNext() { return i.hasPrevious(); }
            public T next() { return i.previous(); }
            public void remove() { i.remove(); }
        };
    }

    public static <T> ReversedIterable<T> reversed(List<T> original) {
        return new ReversedIterable<T>(original);
    }
}
