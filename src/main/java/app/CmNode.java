package app;

import app.graph.DirectedGraph;

public class CmNode extends DirectedGraph.Node {
    public String osmId;

    public CmNode(String osmId) {
        this.osmId = osmId;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof CmNode)) {
            return false;
        }
        CmNode other = (CmNode) obj;
        return other.osmId.equals(this.osmId);
    }

    @Override
    public int hashCode() {
        return osmId.hashCode();
    }

    @Override
    public String toString() {
        return "osm : " + osmId;
    }
}
