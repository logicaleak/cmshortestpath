package app.graph;

import java.util.*;


/**
 * Data structure that represents a directed graph
 * It is a mutable structure
 *
 * @param <N>
 * @param <E>
 */
public class DirectedGraph<N extends DirectedGraph.Node, E extends DirectedGraph.Edge> implements Graph<N, E>, Iterable<DirectedGraph.Node> {
    protected Map<N, List<E>> edgeListMap;
    protected Set<N> nodeSet;

    public DirectedGraph(Map<N, List<E>> edgeListMap) {
        this.edgeListMap = edgeListMap;
    }

    public DirectedGraph() {
        this.edgeListMap = new HashMap<N, List<E>>();
    }

    @Override
    public List<E> getEdges(N node) {
        if (node != null) {
            return this.edgeListMap.get(node);
        }
        return null;
    }

    protected Map<N, List<E>> getEdgeListMap() {
        return edgeListMap;
    }

    /**
     * Adds node to the app.graph unless it already exists
     * @param node Node to add
     */
    @Override
    public void addNode(N node) {
        List<E> edgeList = edgeListMap.get(node);
        if (edgeList == null) {
            edgeList = new LinkedList<>();
            edgeListMap.put(node, edgeList);
        }
    }


    @Override
    public void addEdge(N fromNode, E edge) {
        List<E> edgeList = edgeListMap.get(fromNode);
        if (edgeList != null) {
            edgeList.add(edge);
        }
    }

    @Override
    public Iterator<Node> iterator() {
        Iterator<Node> it = new Iterator<Node>() {

            private Iterator<Map.Entry<N, List<E>>> iterator = DirectedGraph.this.edgeListMap.entrySet().iterator();

            @Override
            public boolean hasNext() {
                return this.iterator.hasNext();
            }

            @Override
            public Node next() {
                return this.iterator.next().getKey();
            }
        };
        return it;
    }

    @Override
    public Integer getNodeNumber() {
        return this.edgeListMap.size();
    }

    public static class Edge<N2 extends DirectedGraph.Node> implements Comparable<Edge<N2>>{
        public final N2 to;
        public final Integer cost;

        public Edge(N2 to, Integer cost) {
            this.to = to;
            this.cost = cost;
        }

        public int compareTo(Edge<N2> o) {
            return this.cost.compareTo(o.cost);
        }

        @Override
        public String toString() {
            return "to : " + to.toString() + ", cost : " + cost.toString();
        }
    }

    public static class Node {

    }
}
