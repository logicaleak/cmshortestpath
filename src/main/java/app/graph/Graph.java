package app.graph;

import java.util.List;

public interface Graph<N, E> {
    /**
     * Get edges connected to a node
     * @param node Node that connects the edges
     * @return List of edges
     */
    List<E> getEdges(N node);

    /**
     * Add a node to the graph
     * @param node Node to add
     */
    void addNode(N node);

    /**
     * Add an edge to the graph
     * @param fromNode The node to which the edges will connect
     * @param edge
     */
    void addEdge(N fromNode, E edge);

    /**
     * Returns the number of nodes
     * @return number of nodes
     */
    Integer getNodeNumber();
}
